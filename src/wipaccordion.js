var wipwrp = window.wipwrp || {};

wipwrp.accordion = (function($) {


	if(!$) { throw new Error('you need jquery!')}

/* ----------------- private vars ------------------*/

	var	self		= {};

/* ----------------- public methods ---------------*/

	
	self.init = function(opt) {

		var	obj 		= {};

		obj.wrap = opt.wrap == undefined ? '.wrap-accordion' : opt.wrap;
		obj.item = opt.item == undefined ? '.accordion-item' : opt.item;
		obj.trigger = opt.trigger == undefined ? '.accordion-trigger' : opt.trigger;
		obj.itemContent = opt.itemContent == undefined ? '.accordion-content' : opt.itemContent;
		obj.multivisible = opt.multivisible == undefined ? false : opt.multivisible;
		

		// start accordion: 
		if	($(obj.wrap).length !== 0) {
			for (var i = 0; i < $(obj.wrap).length; i++) {
				
				var _rnd = Math.floor(Math.random() * (100));
				if ($(obj.wrap).eq(i).attr('id') == undefined) {
					$(obj.wrap).eq(i).attr('id', 'wrap_accordion_'+i+_rnd);
				};
				_action($(obj.wrap).eq(i).attr('id'), obj);
			};
		}
	}


	function _action(wrap_id, arr){
		
		//SET INIZIAL STATE
		$('#'+wrap_id+' '+arr.item+':not(.active)').find(arr.itemContent).hide();
		$('#'+wrap_id+' '+arr.item+'.active').each(function(){
			$(this).find(arr.trigger).addClass('active');
			if($(this).data('triggerlabel')){
				_oldlabel = $(this).html();
				$(this).html($(this).data('triggerlabel'));
				$(this).data('triggerlabel',_oldlabel);
			}
		})

		$('#'+wrap_id).on('click',arr.trigger, function(e){
			e.preventDefault();
			var objtarget =  e.target;
			var $btn = $(this);
			var $par = $(this).parents(arr.item);

			if(objtarget.nodeName == 'INPUT' || objtarget.nodeName == 'input') {
				//e.preventDefault();
				//e.stopPropagation();
			} else if (!($par.hasClass('active')) && !($par.hasClass('disabled'))) {
				// se non era attivo
				if (arr.multivisible == false) {
					$('#'+wrap_id+' '+arr.item+':not(.disabled) '+arr.itemContent).slideUp();
					$('#'+wrap_id+' '+arr.item+':not(.disabled)').removeClass('active');
				};

				var triggable = $('#'+wrap_id+' '+arr.trigger+'.active');
				triggable.each(function(index, el) {
					$(this).removeClass('active');
					if($(this).data('triggerlabel')){
						_oldlabel = $(this).html();
						$(this).html($(this).data('triggerlabel'));
						$(this).data('triggerlabel',_oldlabel);
					}
				});

				$par.find(arr.itemContent).slideDown('600',function(){
					$(window).trigger('resize');
				});
				$btn.addClass('active');
				$par.addClass('active');

			} else if (!$par.hasClass('disabled')) {
				$par.find(arr.itemContent).slideUp();
				$btn.removeClass('active');
				$par.removeClass('active');
				if($(this).data('triggerlabel')){
					_oldlabel = $(this).html();
					$(this).html($(this).data('triggerlabel'));
					$(this).data('triggerlabel',_oldlabel);
				}
			}

		});
	}


	return self;

})(jQuery);